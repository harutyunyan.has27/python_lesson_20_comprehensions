# Comprehensions
# 9. Flipping key value pairs in a dictionary using dict comprehension (a = {"a":10, "b":20, "c":30}
#  Output {10: 'a', 20: 'b', 30: 'c'}

myDict_1 = {'a': 3, 'b': 4, 'c': 7, 'd': 12, 'h':15, 'r': 17, 'v': 19}
myDict = {k: v for v, k in myDict_1.items()}
print(myDict)