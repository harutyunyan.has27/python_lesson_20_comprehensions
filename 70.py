# Comprehensions
# 1. Use list comprehension to construct a new list but add 6 to each item

lst = [x + 6 for x in range(10) if x + 6]
print(lst)