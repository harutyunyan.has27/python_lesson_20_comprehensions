# Comprehensions
# 4. Find all the numbers from 1-1000 that have a 3 in them

lst = [x for x in range(1000) if x % 3]
print(lst)
