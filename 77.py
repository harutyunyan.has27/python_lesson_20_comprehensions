# Comprehensions
# 8. Dict comprehension value same as key (numbers = [1,2,3,4,9,47] )
# Output {1:1, 2:2, 3:3, 4:4, 9:9, 47:47}

dict_1 = {x: x for x in [3, 4, 7, 12, 15, 17, 19]}
print(dict_1)
