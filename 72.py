# Comprehensions
# 3. Given numbers = range(20), produce a list containing the word 'even' if a number in the numbers is even, and the word 'odd' if the number is odd. Result would look like ['odd','odd', 'even']

obj = ["even" if i % 2 == 0 else "odd" for i in range(20) if i != 0 and i != 2]
print(obj)
